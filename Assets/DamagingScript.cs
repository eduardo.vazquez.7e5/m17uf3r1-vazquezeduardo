using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(AudioSource))]
public class DamagingScript : MonoBehaviour
{
    [SerializeField] private AudioClip auch;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            AudioSource.PlayClipAtPoint(auch, transform.position);
            SceneManager.LoadScene("Game Over");
        }
    }
}
