using System.Collections.Generic;

namespace AddOns.Mg3D_Food
{
    public class Inventory : Singleton<Inventory>
    {
        public List<InventoryItem> listOfItems;
        
        protected override void Awake()
        {
            base.Awake();
            DontDestroyOnLoad(this);
        }

        void Start()
        {
            listOfItems = new List<InventoryItem>();
        }

        // Update is called once per frame
        void Update()
        {
        
        }
    }
}
