using System;
using UnityEngine;

namespace AddOns.Mg3D_Food
{
    [Serializable]
    public class InventoryItem
    {
        [SerializeField] internal GameObject worldObject;
        [SerializeField] internal GameObject bodyObject;

        internal void PickUp()
        {
            bodyObject.GetComponent<MeshRenderer>().enabled = true;
        }
    }
}
