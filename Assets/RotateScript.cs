using Cinemachine;
using UnityEngine;

public class RotateScript : MonoBehaviour
{
    private CinemachineFreeLook _cine;
    public float rotatingSpeed = 90f;
    // Start is called before the first frame update
    void Awake()
    {
        _cine = gameObject.GetComponent<CinemachineFreeLook>();
    }

    // Update is called once per frame
    void Update()
    {
        _cine.m_XAxis.Value = (_cine.m_XAxis.Value + rotatingSpeed * Time.deltaTime) % 360;
    }
}
