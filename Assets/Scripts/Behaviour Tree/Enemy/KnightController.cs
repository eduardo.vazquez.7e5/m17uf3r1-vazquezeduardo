using System.Collections.Generic;
using System.Linq;
using BehaviorTree;
using BehaviorTree.Enemy;
using UnityEngine;
using UnityEngine.AI;

public class KnightController : BehaviorTree.Tree
{
    [SerializeField] internal List<GameObject> puntosDePatrulla;
    [SerializeField] internal GameObject player;
    [SerializeField] internal GameObject thisKnight;
    protected override Node SetupTree()
    {
        Node root = new Selector(new List<Node>
        {
            new Atacar(player, transform, thisKnight.GetComponent<NavMeshAgent>(), thisKnight.GetComponent<Animator>()),
            new Sequence( new List<Node>
                {
                    new CheckPlayerIsNear(player, transform),
                    new GoToPlayer(player, thisKnight.GetComponent<NavMeshAgent>(), thisKnight.GetComponent<Animator>())
                }
            ),
            new Patrullar(
                transform, 
                thisKnight.GetComponent<NavMeshAgent>(),
                thisKnight.GetComponent<Animator>(),
                puntosDePatrulla
            ),
        });

        return root;
    }
}
