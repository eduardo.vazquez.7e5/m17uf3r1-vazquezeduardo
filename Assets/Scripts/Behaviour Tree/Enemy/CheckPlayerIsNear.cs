using UnityEngine;

namespace BehaviorTree.Enemy
{
    public class CheckPlayerIsNear : Node
    {
        private GameObject _playerGo;
        private Transform _knightTr;
        public CheckPlayerIsNear(GameObject playerGo, Transform knightTr)
        {
            _playerGo = playerGo;
            _knightTr = knightTr;
        }

        public override NodeState Evaluate()
        {
            var vecDistance = _playerGo.transform.position - _knightTr.position;
            var distance = new Vector2(vecDistance.x, vecDistance.z).magnitude;
            Debug.Log(distance);
            if (distance < 10)
            {
                state = NodeState.SUCCESS;
                return state;
            }
            state = NodeState.FAILURE;
            return state;
        }
    }
}