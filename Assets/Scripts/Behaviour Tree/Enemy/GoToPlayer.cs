using UnityEngine;
using UnityEngine.AI;

namespace BehaviorTree.Enemy
{
    public class GoToPlayer: Node
    {
        private GameObject _playerTr;
        private NavMeshAgent _agent;
        private Animator _animator;
        private static readonly int Walking = Animator.StringToHash("Walking");
        private static readonly int Attacking = Animator.StringToHash("Attacking");

        public GoToPlayer(GameObject playerTr, NavMeshAgent agent, Animator animator)
        {
            _agent = agent;
            _playerTr = playerTr;
            _animator = animator;
        }

        public override NodeState Evaluate()
        {
            Debug.Log("siguele siguele");
            _agent.speed = 3.5f;
            _agent.destination = _playerTr.transform.position;
            _animator.SetBool(Walking, true);
            _animator.SetBool(Attacking, false);
            state = NodeState.SUCCESS;
            return state;
        }
    }
}