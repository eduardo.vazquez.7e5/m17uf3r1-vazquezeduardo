using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace BehaviorTree.Enemy
{
    public class Patrullar : Node
    {
        private int index = 0;
        private Transform knightTr;
        private List<GameObject> patrullarTr;
        private NavMeshAgent _agent;
        private Animator _animator;
        private static readonly int Walking = Animator.StringToHash("Walking");
        private static readonly int Attacking = Animator.StringToHash("Attacking");

        public Patrullar(Transform knightPos, NavMeshAgent agent, Animator animator, List<GameObject> patrullarPos)
        {
            knightTr = knightPos;
            patrullarTr = patrullarPos;
            _agent = agent;
            _animator = animator;
        }
    
        public override NodeState Evaluate()
        {
            Debug.Log("Patrullando sí");
            _agent.speed = 3.5f;
            _animator.SetBool(Walking, true);
            _animator.SetBool(Attacking, true);
            var currentObjective = patrullarTr[index].transform.position;
            var vecDistance = knightTr.position - currentObjective;
            var distance = new Vector2(vecDistance.x, vecDistance.z).magnitude;
            if (distance < 1)
                index = (index + 1) % patrullarTr.Count;
            _agent.destination = patrullarTr[index].transform.position;
            state =  NodeState.RUNNING;
            return state;
        }
    }
}