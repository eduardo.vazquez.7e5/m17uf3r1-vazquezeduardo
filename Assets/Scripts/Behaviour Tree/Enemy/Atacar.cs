using UnityEngine;
using UnityEngine.AI;

namespace BehaviorTree.Enemy
{
    public class Atacar :Node
    {
        private GameObject _player;
        private Transform _knightTr;
        private NavMeshAgent _agent;
        private Animator _animator;
        public Atacar(GameObject player, Transform knightTr, NavMeshAgent agent, Animator animator)
        {
            _player = player;
            _knightTr = knightTr;
            _agent = agent;
            _animator = animator;
        }

        public override NodeState Evaluate()
        {
            var vecDistance = _player.transform.position - _knightTr.position;
            var distance = new Vector2(vecDistance.x, vecDistance.z).magnitude;
            if (distance > 2)
            {
                state = NodeState.FAILURE;
                return state;
            }
            
            Debug.Log("Vamo a arrearle");

            _agent.speed = 0f;
            _animator.SetBool("Walking", false);
            _animator.SetBool("Attacking", true);
            state = NodeState.RUNNING;
            return state;
        }
    }
}