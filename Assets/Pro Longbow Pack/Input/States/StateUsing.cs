using StateMachine;
using UnityEngine;

namespace Pro_Longbow_Pack.Input.States
{
    [CreateAssetMenu(fileName = "StateUsing", menuName = "PlayerStateMachine/State/StateUsing")]
    public class StateUsing : State
    {
        private static readonly int Speed = Animator.StringToHash("speed");

        public override void OnStateEnter(StateMachine.StateMachine context)
        {
            if (context is MovementStateController ctx)
                ctx.Animator.SetTrigger("useItem");
            var list = GameManager.Instance.Interactibles;
            list.ForEach(interactible =>
            {
                IInteractible data = interactible.GetComponent<IInteractible>();
                float distance = (context.gameObject.GetComponent<Transform>().position - interactible.GetComponent<Transform>().position).magnitude;
                if(distance < data.Radius())
                    data.Interact();
            });
        }

        public override void OnStateUpdate(StateMachine.StateMachine context)
        {
        }

        public override void OnStateExit(StateMachine.StateMachine context)
        {
        }
    }
}