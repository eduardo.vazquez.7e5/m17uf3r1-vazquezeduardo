using Cinemachine;
using StateMachine;
using UnityEngine;

namespace Pro_Longbow_Pack.Input.States
{
    [CreateAssetMenu(fileName = "StateVictory", menuName = "PlayerStateMachine/State/StateVictory")]
    public class StateVictory : State
    {
        private static readonly int Victory = Animator.StringToHash("victory");

        public override void OnStateEnter(StateMachine.StateMachine context)
        {
            var thirdPerson = GameObject.Find("Third Person Camera");
            if (thirdPerson != null)
            {
                GameObject.Find("Rotating Camera").GetComponent<CinemachineFreeLook>().m_XAxis =
                    thirdPerson.GetComponent<CinemachineFreeLook>().m_XAxis;
            }

            GameObject.Find("Rotating Camera").GetComponent<CinemachineFreeLook>().Priority = 15;
            if (context is MovementStateController ctx)
                ctx.Animator.SetTrigger(Victory);
        }

        public override void OnStateUpdate(StateMachine.StateMachine context)
        {
        }

        public override void OnStateExit(StateMachine.StateMachine context)
        {
            GameObject.Find("Rotating Camera").GetComponent<CinemachineFreeLook>().Priority = 5;
        }
    }
}