using StateMachine;
using UnityEngine;

namespace Pro_Longbow_Pack.Input.States
{
    [CreateAssetMenu(fileName = "StateFalling", menuName = "PlayerStateMachine/State/StateFalling")]
    public class StateFalling : State
    {
        private static readonly int Grounded = Animator.StringToHash("grounded");
        private static readonly int Speed = Animator.StringToHash("speed");

        public override void OnStateEnter(StateMachine.StateMachine context)
        {
            if (context is MovementStateController ctx)
                ctx.Animator.SetBool(Grounded, false);
        }

        public override void OnStateUpdate(StateMachine.StateMachine context)
        {
            if (context is MovementStateController ctx)
            {
                ctx.CalculateSpeed(ctx.Input.run ? ctx.runningSpeed : ctx.walkingSpeed);
                ctx.Animator.SetFloat(Speed, ctx.Speed);
                if (ctx.autoOrient || ctx.Input.movement != Vector2.zero)
                    ctx.Orient();
                ctx.Move();
            }
        }

        public override void OnStateExit(StateMachine.StateMachine context)
        {
            if (context is MovementStateController ctx)
                ctx.Animator.SetBool(Grounded, true);
        }
    }
}
