using StateMachine;
using UnityEngine;

namespace Pro_Longbow_Pack.Input
{
    [CreateAssetMenu(fileName= "StateCrouching", menuName = "PlayerStateMachine/State/StateCrouching")]
    public class StateCrouching : State
    {
        private static readonly int Crouching = Animator.StringToHash("crouching");
        private static readonly int Speed = Animator.StringToHash("speed");

        public override void OnStateEnter(StateMachine.StateMachine context)
        { 
            if (context is MovementStateController ctx)
                ctx.Animator.SetBool(Crouching, true);
        }

        public override void OnStateUpdate(StateMachine.StateMachine context)
        {
            if (context is MovementStateController ctx)
            {
                ctx.CalculateSpeed(ctx.crouchingSpeed);
                ctx.Animator.SetFloat(Speed, ctx.Speed);
                if(ctx.autoOrient || ctx.Input.movement != Vector2.zero)
                    ctx.Orient();
                ctx.Move();
            }
        }

        public override void OnStateExit(StateMachine.StateMachine context)
        {
            if (context is MovementStateController ctx)
                ctx.Animator.SetBool(Crouching, false);
        }
    }
}
