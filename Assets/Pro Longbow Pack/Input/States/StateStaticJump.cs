using StateMachine;
using UnityEngine;

namespace Pro_Longbow_Pack.Input.States
{
    [CreateAssetMenu(fileName = "StateStaticJump", menuName = "PlayerStateMachine/State/StateStaticJump")]
    public class StateStaticJump : State
    {
        private static readonly int Speed = Animator.StringToHash("speed");

        public override void OnStateEnter(StateMachine.StateMachine context)
        {
            if (context is MovementStateController ctx)
                ctx.Animator.SetTrigger("staticJump");
        }

        public override void OnStateUpdate(StateMachine.StateMachine context)
        {
            if (context is MovementStateController ctx)
            {
                ctx.CalculateSpeed(ctx.walkingSpeed);
                ctx.Animator.SetFloat(Speed, ctx.Speed);
                if (ctx.autoOrient || ctx.Input.movement != Vector2.zero)
                    ctx.Orient();
                ctx.Move();
            }
        }

        public override void OnStateExit(StateMachine.StateMachine context)
        {

        }
    }
}
