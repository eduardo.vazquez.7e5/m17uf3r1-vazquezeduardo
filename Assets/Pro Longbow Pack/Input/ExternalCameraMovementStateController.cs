using System;
using UnityEngine;

namespace Pro_Longbow_Pack.Input
{
    public class ExternalCameraMovementStateController : MovementStateController
    {

        internal override void Orient()
        {
            MovementAngle = Mathf.Atan2(Input.movement.x, Input.movement.y) * Mathf.Rad2Deg +  cam.eulerAngles.y;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, MovementAngle, ref TurnSmoothVelocity, turnSmoothTime);
            transform.rotation = Quaternion.Euler(0f, angle, 0f);
        }

        internal override void Move()
        {
            Vector3 moveDir = Quaternion.Euler(0f, MovementAngle, 0f) * Vector3.forward;
            Controller.Move(moveDir.normalized * Speed * Time.deltaTime);
        }
    }
}
