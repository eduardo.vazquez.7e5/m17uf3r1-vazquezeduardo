using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace Pro_Longbow_Pack.Input
{
    public class MovementStateController : StateMachine.StateMachine
    {
        internal Animator Animator;
        internal MovementScript Input;
        
        private float _targetAimWeight;
        private float _currentAimWeight;
        private const float AimWeightVelocity = 5f;

        private Transform _tr;
        internal CharacterController Controller;
        public Transform cam;

        public float walkingSpeed = 3f;
        public float runningSpeed = 6f;
        private float _acceleration = 9f;
        public float crouchingSpeed = 2f;
        internal float Speed;
    
        public float turnSmoothTime = 0.1f;
        internal float TurnSmoothVelocity;

        internal float MovementAngle;

        private float _groundedOffset =  0.2f;
        private float _groundedRadius = 0.3f;
        public LayerMask GroundLayers;
        private float _verticalVelocity = 0f;
        public float Gravity;
        internal float TimeNotBeingGrounded;
        internal float TimeToFallDown = 0.5f;

        public bool autoOrient = true;

        private void Awake()
        {
            Animator = GetComponent<Animator>();
            Input = GetComponent<MovementScript>();
            _tr = GetComponent<Transform>();
            Controller = GetComponent<CharacterController>();
            _targetAimWeight = 0f;
            _currentAimWeight = 0f;
        }

        internal override void Update()
        {
            CheckGrounded();
            base.Update();
        }

        /*void UpdateAnimation()
        {
            Animator.SetFloat("speed", Speed);
            Animator.SetBool("running", Input.run);
            Animator.SetBool("crouching", Input.crouch.Value);
        }*/

        internal virtual void Orient()
        {
            MovementAngle = /*Mathf.Atan2(Input.movement.x, Input.movement.y) * Mathf.Rad2Deg + */ cam.eulerAngles.y;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, MovementAngle, ref TurnSmoothVelocity, turnSmoothTime);
            transform.rotation = Quaternion.Euler(0f, angle, 0f);
        }

        internal void CalculateSpeed(float speed)
        {
            /*float targetSpeed = (_input.run ? runningSpeed : walkingSpeed) * _input.movement.magnitude;
            if (_input.crouch.Value)
                targetSpeed = _crouchingSpeed * _input.movement.magnitude;*/

            float targetSpeed = speed * Input.movement.magnitude;
            Speed += _acceleration * (targetSpeed-Speed) * Time.deltaTime;

        }
        
        internal virtual void Move()
        {
            Vector3 moveDir = Quaternion.Euler(0f, MovementAngle, 0f) *
                              new Vector3(Input.movement.x, 0, Input.movement.y);  //* Vector3.forward;
            Controller.Move(moveDir.normalized * Speed * Time.deltaTime);
        }
        
        internal void ShootingController()
        {
            if(Input.cancel.Triggered(true) || Input.run || Input.crouch.Value)
                CancelShooting();
            if (Input.aim.Triggered(out bool aiming))
            {
                if (aiming)
                {
                    Animator.SetBool("aiming", true);
                    _targetAimWeight = 1f;
                }
                else
                {
                    if (Animator.GetBool("aiming") && Animator.GetLayerWeight(1) > 0.8f)
                    {
                        Debug.Log("SHOOT!");
                    }

                    Animator.SetBool("aiming", false);
                    _targetAimWeight = 0f;
                }
            }

            _currentAimWeight += AimWeightVelocity * (_targetAimWeight - _currentAimWeight) * Time.deltaTime;
            Animator.SetLayerWeight(1, _currentAimWeight);
        }
        
        internal void CancelShooting()
        {
            _targetAimWeight = 0f;
            Animator.SetBool("aiming", false);
        }

        void CheckGrounded()
        {
            Vector3 spherePosition = new Vector3(transform.position.x, transform.position.y - _groundedOffset, transform.position.z);
            bool isGrounded = Physics.CheckSphere(spherePosition, _groundedRadius, GroundLayers, QueryTriggerInteraction.Ignore);

            if (!isGrounded)
            {
                TimeNotBeingGrounded += Time.deltaTime;
                _verticalVelocity += Gravity * Time.deltaTime;
                Controller.Move(_verticalVelocity * Vector3.down * Time.deltaTime);
            }
            else
            {
                TimeNotBeingGrounded = 0;
                _verticalVelocity = 0f;
                Animator.SetBool("grounded", true);
            }
        }
    }
}
