using StateMachine;
using UnityEngine;

namespace Pro_Longbow_Pack.Input.Transitions
{
    [CreateAssetMenu(fileName = "AnyToFalling", menuName = "PlayerStateMachine/Transition/AnyToFalling")]
    public class AnyToFalling : TransitionFromAny
    {
        public override bool Trigger(StateMachine.StateMachine context)
        {
            if (context is MovementStateController ctx)
            {
                return ctx.TimeNotBeingGrounded > ctx.TimeToFallDown;
            }
            return false;
        }
    }
}