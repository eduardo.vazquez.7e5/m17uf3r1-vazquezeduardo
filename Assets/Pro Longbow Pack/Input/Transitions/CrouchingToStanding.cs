using StateMachine;
using UnityEngine;

namespace Pro_Longbow_Pack.Input
{
    [CreateAssetMenu(fileName= "CrouchingToStanding", menuName = "PlayerStateMachine/Transition/CrouchingToStanding")]
    public class CrouchingToStanding : TransitionFromTo
    {
        public override bool Trigger(StateMachine.StateMachine context)
        {
            if (context is MovementStateController ctx)
            {
                return !ctx.Input.crouch.Value;
            }
            return false;
        }
    }
}