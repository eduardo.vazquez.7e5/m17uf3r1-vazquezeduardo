using StateMachine;
using UnityEngine;

namespace Pro_Longbow_Pack.Input
{
    [CreateAssetMenu(fileName = "StandingToJump", menuName = "PlayerStateMachine/Transition/StandingToJump")]
    public class StandingToJump : Transition
    {
        [SerializeField] public State Standing;
        [SerializeField] public State StaticJump;
        [SerializeField] public State RunningJump;

        public override bool IsFrom(StateMachine.StateMachine context)
        {
            return context.CurrentState == Standing;
        }

        public override State To(StateMachine.StateMachine context)
        {
            if(context is MovementStateController ctx)
            {
                if (ctx.Speed < 3.5f)
                    return StaticJump;
                return RunningJump;
            }
            return context.CurrentState;
        }

        public override bool Trigger(StateMachine.StateMachine context)
        {
            if (context is MovementStateController ctx)
            {
                return ctx.Input.jump.Triggered(true);
            }
            return false;
        }
    }
}
