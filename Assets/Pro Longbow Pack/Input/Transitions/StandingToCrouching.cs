using StateMachine;
using UnityEngine;

namespace Pro_Longbow_Pack.Input
{
    [CreateAssetMenu(fileName= "StandingToCrouching", menuName = "PlayerStateMachine/Transition/StandingToCrouching")]
    public class StandingToCrouching : TransitionFromTo
    {
        public override bool Trigger(StateMachine.StateMachine context)
        {
            if (context is MovementStateController ctx)
            {
                return ctx.Input.crouch.Value;
            }
            return false;
        }
    }
}
