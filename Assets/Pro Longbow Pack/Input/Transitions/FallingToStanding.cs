using StateMachine;
using UnityEngine;

namespace Pro_Longbow_Pack.Input.Transitions
{
    [CreateAssetMenu(fileName = "FallingToStanding", menuName = "PlayerStateMachine/Transition/FallingToStanding")]
    public class FallingToStanding : TransitionFromTo
    {
        public override bool Trigger(StateMachine.StateMachine context)
        {
            if (context is MovementStateController ctx)
            {
                return ctx.TimeNotBeingGrounded < ctx.TimeToFallDown;
            }
            return false;
        }
    }
}