using StateMachine;
using UnityEngine;

namespace Pro_Longbow_Pack.Input.Transitions
{
    [CreateAssetMenu(fileName= "StandingToVictory", menuName = "PlayerStateMachine/Transition/StandingToVictory")]
    public class StandingToVictory : TransitionFromTo
    {
        public override bool Trigger(StateMachine.StateMachine context)
        {
            if (context is MovementStateController ctx)
            {
                return ctx.Input.victory.Triggered(true);
            }
            return false;
        }
    }
}