using StateMachine;
using UnityEngine;

namespace Pro_Longbow_Pack.Input.Transitions
{
    [CreateAssetMenu(fileName= "StandingToUseItem", menuName = "PlayerStateMachine/Transition/StandingToUseItem")]
    public class StandingToUseItem : TransitionFromTo
    {
        public override bool Trigger(StateMachine.StateMachine context)
        {
            if (context is MovementStateController ctx)
            {
                return ctx.Input.useItem.Triggered(true);
            }
            return false;
        }
    }
}