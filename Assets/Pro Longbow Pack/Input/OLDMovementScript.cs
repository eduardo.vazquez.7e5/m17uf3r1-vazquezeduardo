using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class OLDMovementScript : MonoBehaviour
{
    private Animator _animator;

    // TODO: link movement to Animations here

    PlayerControls _input;

    public Vector2 movement;
    public bool jump;
    public bool run;
    public bool aim;
    public bool cancel;
    public bool crouch;

    private bool wasAiming;
    private bool wasCanceling;
    private bool wasJumping;
    
    private float targetAimWeight;
    private float currentAimWeight;
    private const float aimWeightVelocity = 5f;

    private Transform _tr;
    private CharacterController _controller;
    public Transform cam;

    public float walkingSpeed = 3f;
    public float runningSpeed = 6f;
    private float acceleration = 9f;
    private float crouchingSpeed = 2f;
    private float _speed;
    
    public float turnSmoothTime = 0.1f;
    private float _turnSmoothVelocity;

    private void Awake()
    {
        _input = new PlayerControls();
        _tr = GetComponent<Transform>();
        _controller = GetComponent<CharacterController>();
        _animator = GetComponent<Animator>();

        wasAiming = false;
        wasCanceling = false;
        wasJumping = false;
        targetAimWeight = 0f;
        currentAimWeight = 0f;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        CollectInput();
        Move();
        ShootingController();
        UpdateAnimation();
    }

    private void OnEnable()
    {
        _input.Movement.Enable();
    }

    private void OnDisable()
    {
        _input.Movement.Disable();
    }

    void CollectInput()
    {
        movement = _input.Movement.Move.ReadValue<Vector2>();
        jump = _input.Movement.Jump.ReadValue<float>() > 0.5;
        run = _input.Movement.Run.ReadValue<float>() > 0.5;
        aim = _input.Movement.Aim.ReadValue<float>() > 0.5;
        cancel = _input.Movement.CancelAiming.ReadValue<float>() > 0.5;
        crouch = _input.Movement.Crouch.ReadValue<float>() > 0.5;
    }

    void UpdateAnimation()
    {
        _animator.SetFloat("speed", _speed);
        _animator.SetBool("running", run);
        _animator.SetBool("crouching", crouch);
        
    }

    void Move()
    {
        Vector3 direction = new Vector3(movement.x, 0, movement.y);
        
        float targetSpeed = (run ? runningSpeed : walkingSpeed) * movement.magnitude;
        if (crouch)
            targetSpeed = crouchingSpeed * movement.magnitude;

        if (!crouch && jump && ! wasJumping)
        {
            _animator.SetTrigger("jumping");
        }
        
        _speed += acceleration * (targetSpeed-_speed) * Time.deltaTime;
        
        if(_speed >= 0.03f)
        {
            float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref _turnSmoothVelocity, turnSmoothTime);
            transform.rotation = Quaternion.Euler(0f, angle, 0f);

            Vector3 moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
            _controller.Move(moveDir.normalized * _speed * Time.deltaTime);
        }

        wasJumping = jump;
    }

    void ShootingController()
    {
        if(cancel != wasCanceling && cancel || run || crouch)
            CancelShooting();
        if(aim && !wasAiming)
        {
            _animator.SetBool("aiming", true);
            targetAimWeight = 1f;
        }
        else if(!aim && wasAiming)
        {
            if (_animator.GetBool("aiming") && _animator.GetLayerWeight(1) > 0.8f)
            {
                Debug.Log("SHOOT!");
            }
            _animator.SetBool("aiming", false);
            targetAimWeight = 0f;
        }
        wasAiming = aim;
        wasCanceling = cancel;
        currentAimWeight += aimWeightVelocity * (targetAimWeight - currentAimWeight) * Time.deltaTime;
        _animator.SetLayerWeight(1, currentAimWeight);
    }
 
    void CancelShooting()
    {
        targetAimWeight = 0f;
        _animator.SetBool("aiming", false);
    }
}
