using UnityEngine;

namespace Pro_Longbow_Pack.Input
{
    public class MovementScript : MonoBehaviour
    {
        PlayerControls _input;

        public Vector2 movement;
        public Trigger<bool> jump;
        public bool run;
        public Trigger<bool> aim;
        public Trigger<bool> cancel;
        public Trigger<bool> crouch;
        public Trigger<bool> useItem;
        public Trigger<bool> victory;

        private void Awake()
        {
            _input = new PlayerControls();

            aim = new Trigger<bool>(false);
            cancel = new Trigger<bool>(false);
            crouch = new Trigger<bool>(false);
            jump = new Trigger<bool>(false);
            useItem = new Trigger<bool>(false);
            victory = new Trigger<bool>(false);
        }

        // Update is called once per frame
        void Update()
        {
            CollectInput();
        }

        private void OnEnable()
        {
            _input.Movement.Enable();
        }

        private void OnDisable()
        {
            _input.Movement.Disable();
        }

        void CollectInput()
        {
            movement = _input.Movement.Move.ReadValue<Vector2>();
            jump.Value = _input.Movement.Jump.ReadValue<float>() > 0.5;
            run = _input.Movement.Run.ReadValue<float>() > 0.5;
            aim.Value = _input.Movement.Aim.ReadValue<float>() > 0.5;
            cancel.Value = _input.Movement.CancelAiming.ReadValue<float>() > 0.5;
            crouch.Value = _input.Movement.Crouch.ReadValue<float>() > 0.5;
            useItem.Value = _input.Movement.UseItem.ReadValue<float>() > 0.5;
            victory.Value = _input.Movement.Victory.ReadValue<float>() > 0.5;
        }
    }
}
