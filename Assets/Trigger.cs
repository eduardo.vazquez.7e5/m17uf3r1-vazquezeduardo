using UnityEngine;

[System.Serializable]
public class Trigger<T>
{
    [SerializeField]
    private T currentValue;
    private T _previousValue;
    private bool _hasBeenTriggered;

    public T Value
    {
        set
        {  
            currentValue = value;
            if (!currentValue.Equals(_previousValue))
                _hasBeenTriggered = true;
            _previousValue = currentValue;
        }
        get => currentValue;
    }

    public Trigger(T value)
    {
        _previousValue = value;
    }

    public bool Triggered()
    {
        if (_hasBeenTriggered)
        {
            _hasBeenTriggered = false;
            return true;
        }
        return false;
    }
    
    public bool Triggered(T targetValue)
    {
        return Triggered() && currentValue.Equals(targetValue);
    }

    public bool Triggered(out T value)
    {
        value = currentValue;
        return Triggered();
    }

    public bool Triggered(T targetValue, out T value)
    {
        value = currentValue;
        return Triggered(targetValue);
    }
}
