using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;

namespace StateMachine
{
    // [CreateAssetMenu(fileName= "New Transition", menuName = "StateMachine/Transition/Basic Transition")]
    public abstract class Transition : ScriptableObject
    {
        public int priority;
        public abstract bool IsFrom(StateMachine context);
        public abstract State To(StateMachine context);
        public abstract bool Trigger(StateMachine context);

        public virtual void OnTransition(StateMachine context) { }
    }

    public abstract class TransitionFromTo : Transition
    {
        public State from;
        public State to;

        public override bool IsFrom(StateMachine context)
        {
            return context.CurrentState == from;
        }

        public override State To(StateMachine context)
        {
            return to;
        }
    }

    public abstract class TransitionFromAny : Transition
    {
        public State to;

        public override bool IsFrom(StateMachine context)
        {
            return true;
        }

        public override State To(StateMachine context)
        {
            return to;
        }
    }
}
