using UnityEngine;

namespace StateMachine
{
    public abstract class State : ScriptableObject
    {
        public abstract void OnStateEnter(StateMachine context);

        public abstract void OnStateUpdate(StateMachine context);

        public abstract void OnStateExit(StateMachine context);
    }
}
