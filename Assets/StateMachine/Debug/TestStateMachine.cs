using System.Collections;
using UnityEngine;

namespace StateMachine.Debug
{
    public class TestStateMachine : StateMachine
    {
        public string publicSentence;
        public float timeBetweenTransitions;
        public bool shouldITransition;

        internal void WaitToTransition()
        {
            StartCoroutine(Waiter(timeBetweenTransitions));
        }
        
        IEnumerator Waiter(float time)
        {
            yield return new WaitForSeconds(time);
            shouldITransition = true;
        }
    }
}
