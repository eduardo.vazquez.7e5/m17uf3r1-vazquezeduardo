using UnityEngine;

namespace StateMachine.Debug
{
    [CreateAssetMenu(fileName= "SecondState", menuName = "TestStateMachine/State/SecondState")]
    public class SecondState : State
    {
        public override void OnStateEnter(StateMachine context)
        {
            UnityEngine.Debug.Log("i'm the second state i dont wanna talk about it...");
            if (context is TestStateMachine test)
            {
                UnityEngine.Debug.Log(test.publicSentence);
                test.shouldITransition = false;
                test.WaitToTransition();
            }
        }

        public override void OnStateUpdate(StateMachine context)
        {
            //UnityEngine.Debug.Log("2nd...");
        }

        public override void OnStateExit(StateMachine context)
        {
            UnityEngine.Debug.Log("bye..........................................");
        }
    }
}