using System.Collections.Generic;
using UnityEngine;

namespace StateMachine.Debug
{
    [CreateAssetMenu(fileName= "AnyTransition", menuName = "TestStateMachine/Transition/AnyTransition")]
    public class AnyTransition : Transition
    {
        public List<State> states;
        
        public override bool IsFrom(StateMachine context)
        {
            return true;
        }

        public override State To(StateMachine context)
        {
            if (context.CurrentState == states[0])
                return states[1];
            return states[0];
        }

        public override bool Trigger(StateMachine context)
        {
            if (context is TestStateMachine test)
                return test.shouldITransition;
            return false;
        }
    }
}