using System.Collections;
using UnityEngine;

namespace StateMachine.Debug
{
    [CreateAssetMenu(fileName= "InitialState", menuName = "TestStateMachine/State/InitialState")]
    public class InitialState : State
    {
        public override void OnStateEnter(StateMachine context)
        {
            UnityEngine.Debug.Log("YOO I'M THE FIRST STATE I'M SO COOL");
            if (context is TestStateMachine test)
            {
                UnityEngine.Debug.Log(test.publicSentence);
                test.shouldITransition = false;
                test.WaitToTransition();
            }
        }

        public override void OnStateUpdate(StateMachine context)
        {
            //UnityEngine.Debug.Log("1st");
        }

        public override void OnStateExit(StateMachine context)
        {
            UnityEngine.Debug.Log("YOO GOODBYE IT WAS NICE SEEING YOU");
        }


    }
}