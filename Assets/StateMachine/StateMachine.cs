using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace StateMachine
{
    public abstract class StateMachine : MonoBehaviour
    {
        public List<State> states;
        public List<Transition> transitions;
        
        private State _currentState;
        
        public State CurrentState  => _currentState;

        internal virtual void Start()
        {
            _currentState = states[0];
            _currentState.OnStateEnter(this);
        }
        
        internal virtual void Update()
        {
            CheckTransitions();
            _currentState.OnStateUpdate(this);
        }

        private void OnDestroy()
        {
            _currentState.OnStateExit(this);
        }

        void CheckTransitions()
        {

            var validTransitions = transitions
                .FindAll(transition => transition.IsFrom(this) && transition.Trigger(this))
                .OrderBy(transition => transition.priority);

            if (validTransitions.Any())
            {
                var transition = validTransitions.First();
                transition.OnTransition(this);
                TriggerTransition(transition.To(this));
            }
        }

        public void TriggerTransition(State from, State to)
        {
            if(_currentState == from)
                TriggerTransition(to);
        }

        public void TriggerTransition(State to)
        {
            if (states.Contains(to))
            {
                _currentState.OnStateExit(this);
                _currentState = to;
                _currentState.OnStateEnter(this);
            }
        }
    }
}
