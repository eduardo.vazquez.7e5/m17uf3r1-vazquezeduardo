using UnityEngine;
using UnityEngine.Serialization;

[RequireComponent(typeof(AudioSource))]
public class FoodCollectorScript : MonoBehaviour
{
    private float _startingY;
    private Quaternion _startingRot;
    private float _time;
    [SerializeField] private float frequency = 2 * Mathf.PI * 0.5f; 
    [SerializeField] private float amplitude = 0.25f;
    [SerializeField] private float rotFrequency = 360f * 0.25f;
    [SerializeField] private GameObject bodyItem;
    [SerializeField] private AudioClip collectSound;

    private void Start()
    {
        _startingY = transform.position.y;
        _startingRot = transform.rotation;
        _time = 0f;
    }

    private void Update()
    {
        _time += Time.deltaTime;
        var position = transform.position;
        transform.position = new Vector3(position.x, 
            _startingY + amplitude * Mathf.Sin(frequency * _time) ,
            position.z
        );
        transform.rotation = Quaternion.Euler(
            _startingRot.x,
            _startingRot.y + _time * rotFrequency,
            _startingRot.z
        );
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            
            AudioSource.PlayClipAtPoint(collectSound, transform.position);
            bodyItem.GetComponent<MeshRenderer>().enabled = true;
            Destroy(this.gameObject);
        }
    }
}
